package com.kony.compress;
/*
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
*/
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.konylabs.android.KonyMain;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;



public class compresssion {
	static final String NOMBRE_DIRECTORIO = "COMPENSARSEND";
	/*
	public static void main(String[] args) throws IOException{
  // public static void iniciar(String ruta1,String ruta2)  {
	  try{
		  		String ruta1 = "C:\\Users\\soluciones\\Desktop\\digital_image_processing.jpg";
		  		String ruta2 = "digital_image_processing1";
		 // Log.d("StandardLib", "iniciar :: "+ruta1.toString());
		  //Log.d("StandardLib", "iniciar :: "+ruta2.toString());
		      File input = new File(ruta1);//"C:\\Users\\soluciones\\Desktop\\digital_image_processing.jpg");
		      BufferedImage image = ImageIO.read(input);
		   //   Log.d("StandardLib", "iniciar input:: "+input.getAbsolutePath());
		      File compressedImageFile =new  File(ruta1);//"C:\\Users\\soluciones\\Desktop\\compress.jpg");
		      OutputStream os =new FileOutputStream(compressedImageFile);
		    //  Log.d("StandardLib", "iniciar compressedImageFile:: "+compressedImageFile.getAbsolutePath());
		      Iterator<ImageWriter>writers =  ImageIO.getImageWritersByFormatName("jpg");
		      ImageWriter writer = (ImageWriter) writers.next();
		
		      ImageOutputStream ios = ImageIO.createImageOutputStream(os);
		      writer.setOutput(ios);
		
		      ImageWriteParam param = writer.getDefaultWriteParam();
		      
		      param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		      param.setCompressionQuality(0.5f);
		      writer.write(null, new IIOImage(image, null, null), param);
		      
		      os.close();
		      ios.close();
		      writer.dispose();
	  }catch(Exception e){
		//  Log.d("StandardLib", "Exception :: "+e.toString());
	  }
	
   }*/
	
	 public static void iniciar(String filePath,String ruta2)  {
	  try{
		  Log.d("StandardLib", "iniciar nueva :: "+filePath.toString());
		  Log.d("StandardLib", "iniciar :: "+ruta2.toString());
		  KonyMain ctx =  KonyMain.getActivityContext();
		  //File sdCard = Environment.getExternalStorageDirectory();

		 // File directory = new File (sdCard.getAbsolutePath() + "/Pictures");

		  File file = new File(filePath); //or any other format supported

		  FileInputStream streamIn = new FileInputStream(file);
		  Log.d("StandardLib", "iniciar streamIn :: "+streamIn.toString());
		  Bitmap bitmap = BitmapFactory.decodeStream(streamIn); //This gets the image
		  Log.d("StandardLib", "iniciar bitmap :: "+bitmap.getDensity());
		  streamIn.close();
		  Log.d("StandardLib", "iniciar streamIn :: "+streamIn.toString());
		//  Uri selectedImage =  Uri.parse(filePath);
  		//	InputStream is;
  		  Log.d("StandardLib", "iniciar file :: "+file.toString());
  		//	is = ctx.getContentResolver().openInputStream(selectedImage);
  	    //	BufferedInputStream bis = new BufferedInputStream(is);
  	    //	Bitmap bitmap = BitmapFactory.decodeStream(bis);            
  	    	 Log.d("StandardLib", "iniciar bitmap end decode 1 ");
  	    	Save.SaveImage(ctx, bitmap,filePath);
  	    
		 
	  }catch(Exception e){
		  Log.d("StandardLib", "iniciar Exception  :: "+e.toString());
	  }
	 }  
   /**
    * Crea un fichero con el nombre que se le pasa a la funcion y en la ruta
    * especificada.
    * 
    * @param nombreFichero
    * @return
    * @throws IOException
  
   public static File crearFichero(String nombreFichero) throws IOException {
   	File ruta = getRuta();
   	File fichero = null;
   	if (ruta != null){
   		fichero = new File(ruta, nombreFichero);
   	}
   	/**else{
   		fichero = new File(actividad.getFilesDir(), NOMBRE_DOCUMENTO);
   	}
   	return fichero;
   }
   
   
   /**
    * Obtenemos la ruta donde vamos a almacenar el fichero.
    * 
    * @return
    
   public static File getRuta() {

   	// El fichero sera almacenado en un directorio dentro del directorio
   	// Descargas
   	File ruta = null;
   	if (Environment.MEDIA_MOUNTED.equals(Environment
   			.getExternalStorageState())) {
   		ruta = new File(
   				Environment
   						.getExternalStoragePublicDirectory(Environment.MEDIA_MOUNTED_READ_ONLY),
   				NOMBRE_DIRECTORIO);
   				
   		if (ruta != null) {
   			if (!ruta.mkdirs()) {
   				if (!ruta.exists()) {
   					
   					return null;
   				}
   			}
   		}
   	}
   	return ruta;
   }
   
   /**
   public static String compressImage(String imageUri) {

       String filePath = getRealPathFromURI(imageUri);
       Bitmap scaledBitmap = null;

       BitmapFactory.Options options = new BitmapFactory.Options();

//     by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//     you try the use the bitmap here, you will get null.
       options.inJustDecodeBounds = true;
       Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

       int actualHeight = options.outHeight;
       int actualWidth = options.outWidth;

//     max Height and width values of the compressed image is taken as 816x612

       float maxHeight = 816.0f;
       float maxWidth = 612.0f;
       float imgRatio = actualWidth / actualHeight;
       float maxRatio = maxWidth / maxHeight;

//     width and height values are set maintaining the aspect ratio of the image

       if (actualHeight > maxHeight || actualWidth > maxWidth) {
           if (imgRatio < maxRatio) {
               imgRatio = maxHeight / actualHeight;
               actualWidth = (int) (imgRatio * actualWidth);
               actualHeight = (int) maxHeight;
           } else if (imgRatio > maxRatio) {
               imgRatio = maxWidth / actualWidth;
               actualHeight = (int) (imgRatio * actualHeight);
               actualWidth = (int) maxWidth;
           } else {
               actualHeight = (int) maxHeight;
               actualWidth = (int) maxWidth;

           }
       }

//     setting inSampleSize value allows to load a scaled down version of the original image

       options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//     inJustDecodeBounds set to false to load the actual bitmap
       options.inJustDecodeBounds = false;

//     this options allow android to claim the bitmap memory if it runs low on memory
       options.inPurgeable = true;
       options.inInputShareable = true;
       options.inTempStorage = new byte[16 * 1024];

       try {
//         load the bitmap from its path
           bmp = BitmapFactory.decodeFile(filePath, options);
       } catch (OutOfMemoryError exception) {
           exception.printStackTrace();

       }
       try {
           scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
       } catch (OutOfMemoryError exception) {
           exception.printStackTrace();
       }

       float ratioX = actualWidth / (float) options.outWidth;
       float ratioY = actualHeight / (float) options.outHeight;
       float middleX = actualWidth / 2.0f;
       float middleY = actualHeight / 2.0f;

       Matrix scaleMatrix = new Matrix();
       scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

       Canvas canvas = new Canvas(scaledBitmap);
       canvas.setMatrix(scaleMatrix);
       canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//     check the rotation of the image and display it properly
       ExifInterface exif;
       try {
           exif = new ExifInterface(filePath);

           int orientation = exif.getAttributeInt(
                   ExifInterface.TAG_ORIENTATION, 0);
           Log.d("EXIF", "Exif: " + orientation);
           Matrix matrix = new Matrix();
           if (orientation == 6) {
               matrix.postRotate(90);
               Log.d("EXIF", "Exif: " + orientation);
           } else if (orientation == 3) {
               matrix.postRotate(180);
               Log.d("EXIF", "Exif: " + orientation);
           } else if (orientation == 8) {
               matrix.postRotate(270);
               Log.d("EXIF", "Exif: " + orientation);
           }
           scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                   scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                   true);
       } catch (IOException e) {
           e.printStackTrace();
       }

       FileOutputStream out = null;
       String filename = getFilename();
       try {
           out = new FileOutputStream(filename);

//         write the compressed bitmap at the destination specified by filename.
           scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

       } catch (FileNotFoundException e) {
           e.printStackTrace();
       }

       return filename;

   }

   public static String getFilename() {
       File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
       if (!file.exists()) {
           file.mkdirs();
       }
       String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
       return uriSting;

   }

   public static String getRealPathFromURI(String contentURI) {
	   KonyMain ctx = KonyMain.getActivityContext();
       Uri contentUri = Uri.parse(contentURI);
       Cursor cursor = ctx.getContentResolver().query(contentUri, null, null, null, null);
       if (cursor == null) {
           return contentUri.getPath();
       } else {
           cursor.moveToFirst();
           int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
           return cursor.getString(index);
       }
   }

   public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
       final int height = options.outHeight;
       final int width = options.outWidth;
       int inSampleSize = 1;

       if (height > reqHeight || width > reqWidth) {
           final int heightRatio = Math.round((float) height / (float) reqHeight);
           final int widthRatio = Math.round((float) width / (float) reqWidth);
           inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
       }
       final float totalPixels = width * height;
       final float totalReqPixelsCap = reqWidth * reqHeight * 2;
       while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
           inSampleSize++;
       }

       return inSampleSize;
   }
   */
}