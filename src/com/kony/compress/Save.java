package com.kony.compress;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.konylabs.android.KonyMain;

public class Save {
	
    public static KonyMain TheThis;
    public static  String NameOfFolder = "/Nuevacarpeta";
    public static String NameOfFile = "imagen";

    public static void SaveImage(Context context, Bitmap ImageToSave,String ruta) {

        TheThis = KonyMain.getActivityContext();
       
        File file = new File(ruta);//new File(dir, NameOfFile + CurrentDateAndTime + ".jpg");
        Log.d("StandardLib", "iniciar SaveImage file :: "+file.toString());
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            //nuevo 
            double xFactor = 0;
            double width = Double.valueOf(ImageToSave.getWidth());
            Log.d("WIDTH", String.valueOf(width));
            double height = Double.valueOf(ImageToSave.getHeight());
            Log.d("height", String.valueOf(height));
            Log.d("StandardLib", "iniciar SaveImage file :: WIDTH"+ String.valueOf(width)+"height"+ String.valueOf(height));
            long length = file.length(); // Size in KB
           /* if(length>6291456){
                 Toast toast1 = Toast.makeText(context.getApplicationContext(), "mayor 6291456 ::: length  " + length , 100);
                 toast1.show();
                 xFactor = 0.15;
              }
            else{
            	Toast toast1 = Toast.makeText(context.getApplicationContext(), "menor 6291456 ::: length  " + length , 100);
                toast1.show();
                xFactor = 0.25;
            }*/
            xFactor = 0.25;
            Log.d("length", String.valueOf(length));
            Log.d("Nheight", String.valueOf(width*xFactor));
            Log.d("Nweight", String.valueOf(height*xFactor));
            int Nheight = (int) ((xFactor*height));
            int NWidth =(int) (xFactor * width) ; 
            
            Bitmap.createScaledBitmap( ImageToSave,NWidth, Nheight, true);
          //  ImageToSave.createScaledBitmap( ImageToSave,NWidth, Nheight, true);
            //nuevo 
            ImageToSave.compress(Bitmap.CompressFormat.JPEG, 30, fOut);
            Log.d("StandardLib", "iniciar SaveImage length :: "+length +"Nheight"+ String.valueOf(width*xFactor)+"Nweight"+ String.valueOf(height*xFactor));
            fOut.flush();
            fOut.close();
            MakeSureFileWasCreatedThenMakeAvabile(file);
            AbleToSave();
        }

        catch(FileNotFoundException e) {
        	  Log.d("StandardLib", "iniciar SaveImage FileNotFoundException :: "+e.toString());
            UnableToSave();
        }
        catch(IOException e) {
        	  Log.d("StandardLib", "iniciar SaveImage FileNotFoundException :: "+e .toString());
            UnableToSave();
        }

    }

    public static void MakeSureFileWasCreatedThenMakeAvabile(File file){
        MediaScannerConnection.scanFile(TheThis,
            new String[] { file.toString() } , null,
            new MediaScannerConnection.OnScanCompletedListener() {
 
				@Override
				public void onScanCompleted(String arg0, Uri arg1) {
					// TODO Auto-generated method stub
					
				}
            });
    }

 

    public static void UnableToSave() {
       // Toast.makeText(TheThis, "¡No se ha podido guardar la imagen!", Toast.LENGTH_SHORT).show();
    }

    public static void AbleToSave() {
       // Toast.makeText(TheThis, "Imagen guardada en la galería.", Toast.LENGTH_SHORT).show();
    }
}